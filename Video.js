import React from 'react';
import PropTypes from 'prop-types';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import Figure from 'figure';
import Modal from 'modal';
import {isMobile} from 'libs/component';

import Icon from 'icon';
import IconPlay from './play.svg';

import {getModifiers} from 'libs/component';

import './Video.scss';

/**
 * Video
 * @description [Description]
 * @example
  <div id="Video"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Video, {
        title : 'Example Video'
    }), document.getElementById("Video"));
  </script>
 */
class Video extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'video';
		this.playOnLoad = false;

		this.state = {
			isInView: false,
			playing: false,
			playcount: 0,
			isInline: props.isInline
		};
	}

	componentDidMount() {
		// TODO: observe
		console.log('intersection observer');

		/*
		if (!SERVER) {
			if (typeof window === 'object') {
				if (window.IntersectionObserver) {
					const onIntersection = entries => {
						for (const entry of entries) {
							if (entry.isIntersecting) {
								this.setState({isInView: true});
							} else {
								this.setState({isInView: false});
							}
						}
					};

					const observer = new IntersectionObserver(onIntersection);
					observer.observe(this.component);
				}
			}
		}
		*/
	}

	stopOtherVideos() {
		// TODO: stop any other videos that may be playing
		console.log('stop other videos');
	}

	onPlay = () => {
		this.stopOtherVideos();
	};

	onClose = () => {};

	displayInline() {
		const {isInline} = this.props;

		return isMobile() || isInline;
	}

	renderVideo() {
		return <Text content="[Video Placeholder]" />;
	}

	renderPlay() {
		return (
			<button onClick={this.onPlay} className={`${this.baseClass}__play`}>
				<Icon icon={IconPlay} className={`${this.baseClass}__arrow`} />
			</button>
		);
	}

	render() {
		const {caption} = this.props;
		const {playing, playCount, isInline, isInView} = this.state;

		if (isInline) {
			const className = getModifiers(this.baseClass, [
				'inline',
				playing ? 'playing' : 'stopped',
				playCount > 0 ? 'played' : 'notplayed',
				isInView ? 'inview' : null
			]);

			return (
				<div className={className} ref={component => (this.component = component)}>
					<Figure caption={caption}>
						<div className={`${this.baseClass}__preview`}>
							<img ref={video => (this.video = video)} src={this.previewImage} />
							{this.renderPlay()}
							{this.renderVideo()}
						</div>
					</Figure>
				</div>
			);
		}

		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				<Figure caption={caption}>
					<div className={`${this.baseClass}__preview`}>
						<img ref={video => (this.video = video)} src={this.previewImage} />
						{this.renderPlay()}
					</div>
				</Figure>
				<Modal
					isOpen={playing}
					hasChrome={false}
					ref={modal => (this.modal = modal)}
					onClose={this.onClose}
					position="center"
				>
					{this.renderVideo()}
				</Modal>
			</div>
		);
	}
}

Video.defaultProps = {
	id: '',
	isAutoPlay: false,
	isInline: false,
	previewImageUrl: null,
	videoUrl: null,
	caption: null
};

Video.propTypes = {
	caption: PropTypes.string,
	id: PropTypes.string,
	isAutoPlay: PropTypes.bool,
	isInline: PropTypes.bool,
	previewImageUrl: PropTypes.string,
	videoUrl: PropTypes.string
};

export default Video;
